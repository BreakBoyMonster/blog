from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from profiles.tokens import account_activation_token


def get_name(instance, filename):
    """Формируем имя и путь для файлов, добавленных на сервер через filer"""
    return f"dir/{filename}"


def send_email(user, request):
    """Sending activation link to registered user"""
    domain = get_current_site(request).domain
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    subject = 'Активация аккаунта в Блоге'
    template = 'profiles/acc_active_email.jinja'
    to_email = user.email

    message = render_to_string(template, {
        'user': user,
        'domain': domain,
        'uid': uid,
        'token': token,
    })
    email = EmailMessage(
        subject, message, to=[to_email]
    )
    email.send()
    return


def send_link_for_subscribers(to_email, message):
    """Sending link on new topic for subscribers"""
    subject = 'Новая статья на Blog.ua'
    email = EmailMessage(
        subject, message, to=[to_email]
    )
    email.send()
    return
