"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from commons.views import TextPageView
from django.views.generic import RedirectView
from django.conf.urls.i18n import i18n_patterns
from parsing.views import fill_the_database


urlpatterns = i18n_patterns(
    path('adminka/', admin.site.urls),
    path('profiles/', include(('profiles.urls', 'profiles'), namespace='profiles')),
    path('profiles/', include('django.contrib.auth.urls')),
    path('blog/', include(('blog.urls', 'blog'), namespace='blog')),
    path('rosetta/', include('rosetta.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('parsing/', fill_the_database, name='parsing'),
    path('filer/', include('filer.urls')),
    path('<slug:slug>/', TextPageView.as_view(), name='text-page'),

    path('api/v1/blog/', include('blog.api.urls')),
    path('api/v1/auth/', include('rest_framework.urls')),

    path('', RedirectView.as_view(url='/blog/', permanent=True)),

    # path('', include('django.contrib.flatpages.urls')),
    prefix_default_language=False
)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
    if settings.MEDIA_ROOT:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
