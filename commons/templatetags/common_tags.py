from commons.models import Menu

from django.template import Context
from django.template.loader import get_template

# from django import template
# register = template.Library()


# @register.inclusion_tag('commons/menu.jinja')
def header_menu():
    template = get_template('commons/menu.jinja')
    return template.render(Context({'menu': Menu.objects.filter(position='header')}))


def footer_menu():
    template = get_template('commons/menu.jinja')
    return template.render(Context({'menu': Menu.objects.filter(position='footer')}))
