from django.contrib import admin
# from django.contrib.flatpages.admin import FlatPageAdmin
from commons.models import *
from django.utils.translation import ugettext_lazy as _


# class NewTextPageAdmin(admin.StackedInline):
#     model = TextPage
#     verbose_name = _('Содержание')
#     prepopulated_fields = {'slug': ('name',)}
#     readonly_fields = ('creation_date', 'last_change_date')


# class TextPageAdmin(FlatPageAdmin):
#     inlines = [NewTextPageAdmin]
#     fieldsets = (
#         (None, {'fields': ('url', 'title', 'sites')}),
#     )


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'position', 'to_show')


class TextPageAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', 'updated_at')
    prepopulated_fields = {'slug': ('title',)}


# admin.site.unregister(FlatPage)
# admin.site.register(FlatPage, TextPageAdmin)
admin.site.register(TextPage, TextPageAdmin)
