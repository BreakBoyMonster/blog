from django.shortcuts import render
from commons.models import Menu, TextPage
from django.views import generic


class TextPageView(generic.DetailView):
    """Detail view for single text page, return only published, excluding drafts"""
    queryset = TextPage.objects.filter(status='published')
    template_name = 'commons/text_page.jinja'
