from django.db import models
from django.utils.translation import ugettext_lazy as _
from ckeditor_uploader.fields import RichTextUploadingField


class Menu(models.Model):
    """Menu model """
    name = models.CharField(verbose_name=_('title'), max_length=200)
    url = models.CharField(verbose_name=_('url'), max_length=200, unique=True)
    TARGETING = (
        ('new_page', _('new page')),
        ('this_page', _('this page')),
    )
    target = models.CharField(verbose_name=_('target'), max_length=9, choices=TARGETING,
                              blank=True, default='this_page')
    POSITIONS = (
        ('header', _('header')),
        ('footer', _('footer')),
    )
    position = models.CharField(verbose_name=_('position'), max_length=6, choices=POSITIONS)
    to_show = models.BooleanField(verbose_name=_('display'), blank=True, default=True)

    class Meta:
        verbose_name = 'menu'
        verbose_name_plural = 'menus'

    def __str__(self):
        return self.name


class TextPage(models.Model):
    """Text page model"""
    slug = models.SlugField(verbose_name=_('slug'), max_length=300, unique=True, blank=True)
    title = models.CharField(verbose_name=_('title'), max_length=200)
    content = RichTextUploadingField(verbose_name=_('content'), max_length=20000,
                                     blank=True, null=True, help_text=_('content'))
    PAGE_STATUS = (
                ('draft', _('draft')),
                ('published', _('publication')),
                      )
    status = models.CharField(verbose_name=_('status'), max_length=9, choices=PAGE_STATUS)
    created_at = models.DateTimeField(verbose_name=_('creation date'), auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name=_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('text page')
        verbose_name_plural = _('text pages')

    def __str__(self):
        return self.title
