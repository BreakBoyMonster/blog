from django.utils.text import slugify as _slugify
from unidecode import unidecode


def get_picture_name(filename, request):
    """Generate pic name"""
    return f"{request.user}_{filename}"


def get_slug(name, sender):
    """
    Convert string to SLUG-simplified with unique checking.
    """
    slug = _slugify(unidecode(name))
    unique_slug = slug
    num = 1
    while sender.objects.filter(slug=unique_slug).exists():
        unique_slug = '{}-{}'.format(slug, num)
        num += 1
    return unique_slug