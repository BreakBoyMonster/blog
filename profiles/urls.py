from django.urls import path, re_path
from django.contrib.auth import views
from profiles.views import *


urlpatterns = [
    path('<int:pk>/', ProfileDetailView.as_view(), name='profile-detail'),
    path('update/<int:pk>/', ProfileUpdateView.as_view(), name='profile-update'),
    path('sign_up/', ProfileCreateView.as_view(), name='profile-create'),
    re_path(r'activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]+)/$',
            ActivateView.as_view(), name='activate'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('password_change/<int:pk>/', auth_views.PasswordChangeView.as_view(
                        template_name='profiles/passwords/password_change.jinja'), name='password_change'),
    path('password_change/done/', PasswordChangeDoneView.as_view(
                        template_name='profiles/passwords/password_change_done.jinja'), name='password_change_done'),
    path('password_reset/', PassResetView.as_view(), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(
                        template_name='profiles/passwords/password_reset_done.jinja'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
                        template_name="profiles/passwords/password_reset_confirm.jinja"), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(
                        template_name='profiles/passwords/password_reset_complete.jinja'), name='password_reset_complete'),
]
