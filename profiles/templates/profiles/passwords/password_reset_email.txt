{% autoescape off %}
{{ _('hello, we get the request to reset your account password from this email. To reset password follow the link bellow.') }}

{{ protocol }}://{{ domain }}{% url 'password_reset_confirm' uidb64=uid token=token %}

{{ _('link could be used only once. as if you want to reset one more time, please, follow this link and make new request for password reset '}} {{ protocol }}://{{domain}}

{{ _(as if you didn't this request, you could ignore it.

your sincerity, blog.) }}

{% endautoescape %}