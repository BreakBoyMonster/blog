from django import forms
from profiles.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordResetForm
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _


class UserForm(forms.ModelForm):
    """Form to create user"""
    photo = forms.ImageField(label=_('photo'), widget=forms.widgets.FileInput)  # оставляем только кнопку "Выберите файл"
    phone = forms.CharField(label=_('phone'), max_length=20, required=False,
                            widget=forms.TextInput(attrs={'class': 'form-control',
                                                          'placeholder': _('phone'), }))
    last_name = forms.CharField(label=_('surname'), required=False,
                                widget=forms.TextInput(attrs={'class': 'form-control',
                                                              'placeholder': _('surname'), }))
    first_name = forms.CharField(label=_('name'), required=False,
                                 widget=forms.TextInput(attrs={'class': 'form-control',
                                                               'placeholder': _('name'), }))

    class Meta:
        model = User
        fields = ('photo', 'last_name', 'first_name', 'phone',)


class RegForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['email', 'password1', 'password2', 'phone']


# class PassResetForm(PasswordResetForm):
#     pass



# class LoginForm(AuthenticationForm):
#     email = forms.CharField(label='Почта')
#     password = forms.PasswordInput()

# class LoginUserForm(AuthenticationForm):
#     username = forms.CharField(max_length=30, required=False)
#     email = forms.EmailField()
#     password = forms.CharField(widget=forms.PasswordInput)
#     user_cache = None
#
#     def clean(self):
#         try:
#             username = User.objects.get(username=self.cleaned_data['username']).username
#         except User.DoesNotExist:
#             raise forms.ValidationError(_("No such user registered."))
#
#         self.user_cache = authenticate(username=username, password=self.cleaned_data['password'])
#         if self.user_cache is None or not self.user_cache.is_active:
#             raise forms.ValidationError(_("Username or password is incorrect."))
#         return self.cleaned_data
#
#     def get_user(self):
#         return self.user_cache
