from django.contrib.auth import user_logged_in
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractUser
from django.contrib.auth.base_user import AbstractBaseUser
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.core.validators import RegexValidator
from django.shortcuts import render


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a profiles with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        # extra_fields.setdefault('is_active', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


def photo_upload_to(profile, photo):
    """return path, where to put user photos"""
    return f"user_photos/{profile.email}.{photo.split('.')[-1]})"


class User(AbstractUser):
    """Set our user fields, which users will use"""
    username = None
    email = models.EmailField(verbose_name=_('email address'), max_length=255, unique=True)
    phone_regex = RegexValidator(regex=r'^\+?\d{9,15}$',
                                 message=_("number shd be in format: '+999999999' and not longer then 15 characters"))
    phone = models.CharField(verbose_name=_('phone'), validators=[phone_regex], max_length=17,
                             blank=True, null=True,
                             help_text=_("please, fill the number in the format '+999999999'"))
    photo = models.ImageField(verbose_name=_('photo'), upload_to=photo_upload_to, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        try:
            this = User.objects.get(id=self.id)
            if this.photo != self.photo:
                this.photo.delete(save=False)
        except:
            pass
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('profiles:profile-detail', kwargs={'pk': self.pk})


@receiver(user_logged_in)
def post_login(sender, user, request, **kwargs):
    return
