from django.contrib.auth.views import PasswordResetView, PasswordChangeDoneView
from django.urls import reverse_lazy
from django.views import generic
from django.views.generic.edit import FormMixin
from django.views.generic.list import MultipleObjectMixin
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth import views as auth_views
from django.contrib.auth import logout
from django.contrib import messages
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from profiles.tokens import account_activation_token
from profiles.models import *
from profiles.forms import UserForm, RegForm
from blog.models import Topic
from project.utils import send_email


class PassResetView(PasswordResetView):
    template_name = 'profiles/passwords/password_reset.jinja'
    email_template_name = 'profiles/passwords/password_reset_email.txt'


class ProfileDetailView(MultipleObjectMixin, LoginRequiredMixin, generic.DetailView):
    """Profile detail view"""
    model = User
    template_name = 'profiles/profile_detail.jinja'
    paginate_by = 4

    def get_context_data(self, **kwargs):
        object_list = Topic.objects.filter(author=self.request.user)
        context = super().get_context_data(object_list=object_list)
        return context


class ProfileUpdateView(LoginRequiredMixin, generic.UpdateView):
    """Profile update view"""
    model = User
    template_name = 'profiles/profile_update.jinja'
    form_class = UserForm

    def get_success_url(self):
        return reverse('profiles:profile-detail', kwargs={'pk': self.request.user.pk})


class ProfileCreateView(generic.CreateView):
    """Profile create view"""
    model = User
    template_name = 'profiles/profile_signup.jinja'
    form_class = RegForm
    success_url = '/blog/'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.is_active = False
        user.save()
        send_email(user=user, request=self.request)
        messages.success(self.request,
                         'Пожалуйста, подтвердите Вашу почту, чтобы завершить регистрацию.')
        return super().form_valid(form)


class ActivateView(generic.View):
    """Profile activate view"""
    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)

        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            # login(request, user)
            # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
            messages.success(request, 'Аккаунт успешно подтвержден.')
            return redirect('profiles:login')
        else:
            return HttpResponse('Ссылка активации не работает!')


class LoginView(auth_views.LoginView):
    """Logiv view"""
    template_name = 'profiles/login.jinja'

    def dispatch(self, request, *args, **kwargs):
        """
        Вызываем dispatch (он как init) у CBV, вызывается первым для изначальной проверки каких-то значений

        Здесь проверяем, залогиненный пользователь или нет.
        Если залогиненный и переходит на страницу login - делаем logout и отображаем страницу login
        """
        if request.user.is_authenticated:
            logout(request)

        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('profiles:profile-detail', kwargs={'pk': self.request.user.pk})


class LogoutView(auth_views.LogoutView):
    """Logout view"""
    next_page = reverse_lazy('profiles:login')
