import requests
from bs4 import BeautifulSoup as BS
import datetime
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
import re
import time
from django.contrib import messages
import random

# headers = [{'User-Agent':
#             '''Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)
#              Chrome/89.0.4389.90 Safari/537.36}''',
#            'Accept':
#             'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
#            {'User-Agent':
#             'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',
#             'Accept':
#             'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
#            {'User-Agent':
#             'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
#             'Accept':
#             'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
#            {'User-Agent':
#             '''Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
#             ' Chrome/51.0.2704.103 Safari/537.36''',
#             'Accept':
#             'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'},
#            {'User-Agent':
#             '''Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)
#              Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41''',
#             'Accept':
#             'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
headers = [{'User-Agent':
            'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'},
           ]


month = {'01': 'января', '02': 'ферваля', '03': 'марта', '04': 'апреля', '05': 'мая', '06': 'июня',
         '07': 'июля', '08': 'августа', '09': 'сентября', '10': 'октября', '11': 'ноября', '12': 'декабря'}
m = {'January': 'января', 'February': 'ферваля', 'March': 'марта', 'April': 'апреля', 'May': 'мая', 'June': 'июня',
     'July': 'июля', 'August': 'августа', 'September': 'сентября', 'October': 'октября', 'November': 'ноября',
     'December': 'декабря'}
sites = ['habr', 'hackernoon', 'darkreading']


def habr():
    articles = []
    urls = []
    cnt = 1
    session = requests.session()
    base_url = 'https://habr.com/ru/news/'
    while cnt < 2:
        urls.append(base_url + 'page' + str(cnt))
        cnt += 1
        req = session.get(urls[-1], headers=headers[0])
        if req.status_code == 200:
            bsobj = BS(req.content, 'html.parser')
            article_list = bsobj.find_all('article', attrs={'class': 'post'})
            if article_list:
                for article in article_list:
                    art = dict()
                    # creation_date = article.find('span', attrs={'class': 'post__time'}).text.split()
                    # if creation_date[0] == 'сегодня':
                    #     today = datetime.date.today().strftime("%d %m %Y").split()
                    #     today[1] = month[today[1]]
                    #     today = " ".join(today)
                    #     creation_date[0] = today
                    # elif creation_date[0] == 'вчера':
                    #     yesterday = datetime.date.today() - datetime.timedelta(1)
                    #     yesterday = yesterday.strftime("%d %m %Y").split()
                    #     yesterday[1] = month[yesterday[1]]
                    #     yesterday = " ".join(yesterday)
                    #     creation_date[0] = yesterday
                    # art['creation_date'] = " ".join(creation_date)
                    art['author'] = 'habr'
                    art['name'] = article.find('a', attrs={'class': 'post__title_link'}).text
                    article_link = article.find('a', attrs={'class': 'post__title_link'})['href']
                    category = []
                    cat = article.find_all('a', attrs={'class': 'inline-list__item-link'})
                    for c in cat:
                        category.append(c.text)
                    art['category'] = category
                    art['description'] = article.find(
                            'div',
                            attrs={'class': 'post__text-html'}
                        ).text.replace('\xa0', ' ').replace('\n', ' ').replace('\r', ' ').strip()
                    photo = 'None'
                    try:
                        photo = article.find('div', attrs={'class': 'post__body'}).find('img')['src']
                    except (TypeError, AttributeError):
                        pass
                    art['photo'] = photo
                    art['content'] = 'None'
                    req = session.get(article_link, headers=headers[0])
                    if req.status_code == 200:
                        bsobj = BS(req.content, 'html.parser')
                        content = bsobj.find(
                                'div',
                                attrs={'class': 'post__text'}
                            ).text.replace('\xa0', ' ').replace('\n', ' ').replace('\r', ' ').strip()
                        content = ". ".join([x.strip() for x in content.split('.')])
                        art['content'] = content
                    articles.append(art)
        else:
            break
    return articles


def krebsonsecurity():
    articles = []
    urls = []
    cnt = 1
    session = requests.session()
    base_url = 'https://krebsonsecurity.com/'
    while cnt < 2:
        urls.append(base_url + 'page' + str(cnt))
        cnt += 1
        req = session.get(urls[-1], headers=headers[0])
        if req.status_code == 200:
            bsobj = BS(req.content, 'html.parser')
            article_list = bsobj.find_all('article', attrs={'class': 'post'})
            if article_list:
                for article in article_list:
                    art = dict()
                    # date = article.find('span', attrs={'class': 'date'}).text.split()
                    # art['creation_date'] = str(date[1][:-1]) + " " + m[date[0]] + " " + date[2]
                    art['author'] = 'krebsonsecurity'
                    t = article.find('h2', attrs={'class': 'entry-title'})
                    art['name'] = t.find('a').text
                    article_link = t.find('a')['href']
                    art['category'] = ['IT']
                    art['description'] = article.find(
                        'div', attrs={'class': 'entry-content'}
                            ).text[:-20].replace('\xa0', ' ').replace('\n', ' ').replace('\r', ' ').strip()
                    photo = 'None'
                    try:
                        a = article.find('div', attrs={'class': 'entry-content'})
                        photo = a.find('img')['src']
                    except (TypeError, AttributeError):
                        pass
                    art['photo'] = photo
                    art['content'] = 'None'
                    req = session.get(article_link, headers=headers[0])
                    if req.status_code == 200:
                        bsobj = BS(req.content, 'html.parser')
                        content = bsobj.find(
                            'div',
                            attrs={'class': 'entry-content'}
                        ).text.replace('\xa0', ' ').replace('\n', ' ').replace('\r', ' ').strip()
                        art['content'] = ". ".join([x.strip() for x in content.split('.')])
                    articles.append(art)
        else:
            break
    return articles


# darkreading()


def hackernoon():
    articles = []
    urls = []
    cnt = 1
    base_url = 'https://hackernoon.com/tagged/hackernoon-top-story'
    opts = webdriver.ChromeOptions()
    opts.add_argument('headless')
    browser = webdriver.Chrome(ChromeDriverManager().install(), options=opts)
    while cnt < 2:
        page_articles = []
        urls.append(base_url + '?page=' + str(cnt))
        cnt += 1
        browser.get(urls[-1])

        try:
            WebDriverWait(browser, timeout=10).until(
                lambda x: x.find_element_by_class_name('date'))
        except TimeoutException:
            break

        for article in browser.find_elements_by_css_selector('article.Card-sc-4llv5q-0'):
            try:
                # date = article.find_elements_by_class_name('date')[0].text.split('/')
                # creation_date = date[1] + ' ' + month[date[0]] + ' ' + '20' + date[2]
                name = article.find_elements_by_class_name('title-wrapper')[0].text
                article_link = article.find_elements_by_class_name('title-wrapper')[0]. \
                    find_element_by_tag_name('a').get_attribute('href')
                page_articles.append({'name': name, 'article_link': article_link,
                                      'author': 'hackernoon', 'category': ['IT'],
                                      'description': 'None'})
            except IndexError:
                pass

        for article in page_articles:
            browser.get(article['article_link'])
            article['photo'] = 'None'
            article['content'] = 'None'
            try:
                text = browser.find_element_by_css_selector('.ChiNr').text.replace('\n', ' ').replace('\'', '')
                start = browser.find_element_by_css_selector('div.profile').text.split()[-1]
                finish = browser.find_element_by_css_selector('div.author-info span').text
                start_index = text.find(start)
                finish_index = text.find(finish)
                article['content'] = text[start_index + len(start): finish_index - 2].strip()
                article['photo'] = browser.find_element_by_css_selector('.fullscreen img').get_attribute('src')
            except NoSuchElementException:
                # print('\n', '*' * 50, 'NoSuchElementException', '*' * 50, '\n')
                pass
            finally:
                del article['article_link']
        articles = articles + page_articles

    return articles


def get_articles():
    articles = list() + habr() + hackernoon() + krebsonsecurity()
    return articles
