import requests
from django.http import HttpResponseForbidden
from blog.models import *
from parsing.parser import get_articles
from django.shortcuts import redirect
from django.db import IntegrityError
from django.core.files import File
from tempfile import NamedTemporaryFile
from filer.models import Image
from django.core.exceptions import ObjectDoesNotExist
from commons.utils import get_slug


def fill_the_database(request):
    """Function calling parser, get data and fill it in DB"""
    if request.user.is_superuser:
        users_set = {'krebsonsecurity', 'habr', 'hackernoon'}
        for user in users_set:
            try:
                User.objects.create(email=user + '@gmail.com', password='12344321qwerty')
            except IntegrityError:
                pass
        list_of_articles = get_articles()  # call func to parse
        for article in list_of_articles:
            if len(article['description']) > 250:
                article['description'] = article['description'][:250]
                for index in range(250, 1, -1):
                    if '.' in article['description']:
                        if article['description'][index - 3] == '.':
                            article['description'] = article['description'][:index - 2]
                            break
                        else:
                            article['description'] = article['description'][:index - 3]
                    else:
                        if article['description'][index - 3] == ',':
                            article['description'] = article['description'][:index - 2]
                            article['description'] = article['description'].replace(article['description'][-1], '.')
                            break
                        else:
                            article['description'] = article['description'][:index - 3]
            try:
                topic = Topic.objects.create(name=article['name'],
                                             slug=get_slug(name=article['name'], sender=Topic),
                                             description=article['description'],
                                             content=article['content'],
                                             author=User.objects.get(email=article['author'] + '@gmail.com'))
                topic.save()
                for category in article['category']:
                    try:
                        Category.add_root(name=category, slug=get_slug(name=category, sender=Category))
                    except IntegrityError:
                        pass
                    try:
                        topic.category.add(Category.objects.get(name=category))
                    except ObjectDoesNotExist:
                        print('ЭТОЙ НЕТУ', category)
                if article['photo']:
                    file_name = article['photo'].split('/')[-1]
                    response = requests.get(article['photo'], stream=True)
                    temp_file = NamedTemporaryFile()
                    temp_file.write(response.content)
                    img = Image.objects.create(
                        original_filename=file_name,
                        file=File(temp_file, file_name))
                    topic.photo = img
                    topic.save()
            except IntegrityError:
                pass
        return redirect('blog:blog')
    else:
        return HttpResponseForbidden('You are not able to parse new articles.')


# fill_the_database(get_articles())
