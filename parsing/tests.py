import django.db.models
from django.test import TestCase, TransactionTestCase
from django.shortcuts import reverse
from parsing.views import get_slug
from blog.models import Category, Topic
from profiles.models import User
from parsing.parser import get_articles
from django.core.files import File
from django.core.files.images import ImageFile
import requests


class ViewTest(TransactionTestCase):
    def test_parser(self):
        """Check is parser get the page"""
        response = self.client.get(reverse('parsing'))
        print(response)
        self.assertEqual(response.status_code, 302)

    def setUp(self):
        """Prepare data"""
        users_set = {'krebsonsecurity', 'habr', 'hackernoon'}
        for user in users_set:
            User.objects.create(email=user + '@gmail.com', password='12344321qwerty')
        Category.add_root(name='айтишка', slug=get_slug(name='айтишка', sender=Category))
        categories = Category.objects.all()
        self.lst_of_existing_categories = []
        self.set_of_parsed_categories = set()
        self.parsed_list_of_articles = get_articles()
        for cat in categories:
            self.lst_of_existing_categories.append(cat.name)
        for article in self.parsed_list_of_articles:
            for category in article['cate0gory']:
                self.set_of_parsed_categories.add(category)
        for category in self.set_of_parsed_categories:
            if category not in self.lst_of_existing_categories:
                Category.add_root(name=category, slug=get_slug(name=category, sender=Category))
        self.categories = set.union(self.set_of_parsed_categories, set(self.lst_of_existing_categories))

    def test_fill_new_categories_in_database(self):
        """Adding new categories to DB"""
        Category.add_root(name='тестовая категория', slug=get_slug(name='тестовая категория', sender=Category))
        self.categories.add('тестовая категория')
        c = Category.objects.all()
        self.assertEqual(len(self.categories), len(c))

    def test_fill_new_topic(self):
        """Adding new topic in DB using real parser"""
        for article in self.parsed_list_of_articles:
            if len(article['description']) > 250:
                article['description'] = article['description'][:250]
                for index in range(250, 1, -1):
                    if '.' in article['description']:
                        if article['description'][index - 3] == '.':
                            article['description'] = article['description'][:index - 2]
                            break
                        else:
                            article['description'] = article['description'][:index - 3]
                    else:
                        if article['description'][index - 3] == ',':
                            article['description'] = article['description'][:index - 2]
                            article['description'] = article['description'].replace(article['description'][-1], '.')
                            break
                        else:
                            article['description'] = article['description'][:index - 3]
            topic = Topic.objects.create(name=article['name'],
                                         slug=get_slug(name=article['name'], sender=Topic),
                                         description=article['description'],
                                         content=article['content'],
                                         author=User.objects.get(email=article['author'] + '@gmail.com'),
                                         )
            topic.save()
            for category in article['category']:
                topic.category.add(Category.objects.get(name=category))
        self.assertEqual(len(self.parsed_list_of_articles), len(Topic.objects.all()))
