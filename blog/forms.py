from django import forms
from django.utils.translation import ugettext_lazy as _

from blog.models import Review, Topic


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = ('mark', 'comment')


class TopicSearchForm(forms.Form):
    q = forms.CharField(label=_('search'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['q'].widget.attrs = {'placeholder': _('search')}
