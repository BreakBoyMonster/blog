from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404, redirect
from rest_framework import generics, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from blog.api.serializers import (TopicSerializer, CategorySerializer,
                                  UserSerializer, LikeSerializer, SubscribeSerializer, ReviewSerializer)
from blog.models import Topic, Category, FavouriteTopic, Subscribe, Review
from profiles.models import User
from blog.api.permissions import IsOwnerOrAdminOrReadOnly, IsAdminOrReadOnly


class ReviewViewSet(viewsets.ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = 'slug'
    permission_classes = [IsAdminOrReadOnly]


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]


class TopicViewSet(viewsets.ModelViewSet):  # instead ReadOnlyModelViewSet
    queryset = Topic.objects.prefetch_related('category', 'recommended_topics').select_related('author').all()
    serializer_class = TopicSerializer
    lookup_field = 'slug'
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrAdminOrReadOnly]  # IsAuthenticatedOrReadOnly

    def get_serializer_class(self):
        """Depending on which action it is - choose right serializer"""
        if self.action == 'like_topic':
            return LikeSerializer
        elif self.action == 'subscribe':
            return SubscribeSerializer
        else:
            return self.serializer_class

    @action(methods=['post'], detail=False)
    def like_topic(self, request):
        """Method to like/dislike single topic"""
        if request.user.is_authenticated:
            favourite_topic, created = FavouriteTopic.objects.get_or_create(
                user=request.user,
                topic_id=request.data['topic'])
            if not created:
                favourite_topic.delete()
        return redirect(request.META['HTTP_REFERER'])

    @action(methods=['post'], detail=False)
    def subscribe(self, request):
        """Method to subscribe/unsubscribe on user to get messages on email about new articles"""
        if request.user.is_authenticated:
            who = request.user
            on_whom = User.objects.get(pk=request.data['on_whom'])
            if who == on_whom:
                pass
            else:
                subscribe, created = Subscribe.objects.get_or_create(who=who, on_whom=on_whom)
                if not created:
                    subscribe.delete()
        return redirect(request.META['HTTP_REFERER'])
