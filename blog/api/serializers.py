from django.contrib.auth import get_user_model
from rest_framework import serializers
from commons.utils import get_slug
from blog.models import Category, Topic, FavouriteTopic, Subscribe, Review


class UserSerializer(serializers.ModelSerializer):
    """Serializer to convert python format data into JSON"""
    class Meta:
        model = get_user_model()
        fields = ('email', 'password', 'phone', 'photo', 'pk')


class ReviewSerializer(serializers.ModelSerializer):
    """Serializer to convert python format data into JSON"""
    user = serializers.SlugRelatedField(slug_field='email', read_only=True)
    topic = serializers.SlugRelatedField(slug_field='name', queryset=Topic.objects.all())

    class Meta:
        model = Review
        exclude = ('creation_date', 'last_change_date')

    def create(self, validated_data):
        if self.context['request'].user:
            validated_data['user'] = self.context['request'].user
        review = Review.objects.create(**validated_data)
        return review


class LikeSerializer(serializers.ModelSerializer):
    """Serializer to convert python format data into JSON"""
    class Meta:
        model = FavouriteTopic
        fields = ('topic',)


class SubscribeSerializer(serializers.ModelSerializer):
    """Serializer to convert python format data into JSON"""
    class Meta:
        model = Subscribe
        fields = ('on_whom',)


class CategorySerializer(serializers.ModelSerializer):  # instead PrimaryKeyRelatedField
    """Serializer to convert python format data into JSON"""
    class Meta:
        model = Category
        fields = ('name',)

    def create(self, validated_data):
        return Category.add_root(name=validated_data['name'],
                                 slug=get_slug(name=validated_data['name'], sender=Category))


class TopicSerializer(serializers.ModelSerializer):
    """Serializer to convert python format data into JSON"""
    class Meta:
        model = Topic
        exclude = ('creation_date', 'last_change_date')
        read_only_fields = ('slug', 'number _of_reviews', 'number_of_likes', 'number_of_reviews',
                            'recommended_topics', 'rating', 'author')

    # category = serializers.SerializerMethodField()
    # category = serializers.SlugRelatedField(slug_field='name', many=True, read_only=True)
    author = serializers.SlugRelatedField(slug_field='email', read_only=True)

    # def get_category(self, obj):
    #     return CategorySerializer(instance=obj.category.all(), many=True).data

    def create(self, validated_data):
        # print(validated_data)
        validated_data['slug'] = get_slug(name=validated_data['name'], sender=Topic)
        validated_data['author'] = self.context['request'].user
        categories = validated_data.pop('category')
        topic = Topic.objects.create(**validated_data)
        for cat in categories:
            topic.category.add(Category.objects.get(name=cat))
        return topic
