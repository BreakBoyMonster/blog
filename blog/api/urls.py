from django.urls import path
from blog.api.views import *


from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'topics', TopicViewSet)
router.register(r'categories', CategoryViewSet)
# router.register(r'change_topics', Create_or_update_or_delete_topicView)
router.register(r'reviews', ReviewViewSet)
router.register(r'users', UserViewSet)

urlpatterns = router.urls
# urlpatterns = (
#     # path('topic/detail/', TopicDetailView.as_view({'get': 'slug'}), name='topic-detail'),
#     # path('topic/detail/', TopicDetailView.as_view(), name='topic-detail'),
#     # path('blog/', BlogView.as_view({'get': 'list'})),
#     # path('topics/', TopicListView.as_view()),
#     path('topics/<slug:slug>/', TopicViewSet.as_view()),
# )
