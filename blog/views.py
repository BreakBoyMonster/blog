from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.views.generic.edit import FormMixin, FormView
from django.views.generic.list import MultipleObjectMixin
from django.views import View

from blog.models import Topic, FavouriteTopic, Review, Category, Subscribe
from profiles.models import User
from django.urls import reverse, reverse_lazy
from urllib.parse import urlparse
from django.contrib import auth
from blog.forms import ReviewForm, TopicSearchForm
from django_filters import FilterSet, OrderingFilter, CharFilter, ModelMultipleChoiceFilter
from django_filters.views import FilterView


class TopicFilter(FilterSet):
    """Filter ('creation_date', 'rating') and ordering ('author', 'category') for topics"""
    ordering = OrderingFilter(fields=('creation_date', 'rating'))
    # category = CharFilter(lookup_expr='iexact')
    # category = ModelMultipleChoiceFilter(
    #     label='category',
    #     field_name='category__slug',
    #     to_field_name='slug',
    #     queryset=Category.objects.filter(parent=None),
    #     # widget=forms.CheckboxSelectMultiple()
    # )

    # subcategory = ModelMultipleChoiceFilter(
    #     label='subcategory',
    #     field_name='category__slug',
    #     to_field_name='slug',
    #     queryset=Category.objects.exclude(parent=None),
    #     # widget=forms.CheckboxSelectMultiple()
    # )

    class Meta:
        model = Topic
        fields = ('author', 'category')


class BlogView(FilterView):
    """Main view blog (including all topics)"""
    model = Topic
    filterset_class = TopicFilter
    template_name = 'blog/blog.jinja'
    paginate_by = 2


class TopicView(MultipleObjectMixin, FormMixin, generic.DetailView):
    """Detail topic view"""
    model = Topic
    template_name = 'blog/topic.jinja'
    form_class = ReviewForm
    paginate_by = 2

    def get_context_data(self, **kwargs):
        object_list = Review.objects.filter(topic=self.get_object())
        context = super().get_context_data(object_list=object_list)
        context['topic'] = self.get_object()
        return context

    def get_success_url(self):
        return reverse('blog:topic-detail', kwargs={'slug': self.get_object().slug})

    def post(self, request, *args, **kwargs):
        """
        Создаем метод, который берет форму, проверяет валидность и вызывает соотвествующий метод
        """
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        """
        Переопределяем метод для подготовки объекта на сохранение в БД
        """
        obj = form.save(commit=False)  # сохраняем данные из формы в переменную не записывая в БД
        if self.request.user.is_authenticated:
            obj.user = self.request.user
        obj.topic = self.get_object()
        obj.save()  # сохраняем в БД до заполненую форму (объект)
        return super().form_valid(form)  # вызываем родительный form_valid к нашей форме (у FormMixin берем)


class FavouriteTopicView(View):
    """
    View to check, is single topic already in user favourites,
    if not - it add topic to list, if yes - delete topic from list
    """
    model = FavouriteTopic

    def get(self, request, slug):
        user = auth.get_user(request)
        t = Topic.objects.get(slug=slug)
        favourite_topic, created = self.model.objects.get_or_create(user=user, topic_id=t.pk)
        # if no new bookmark was created,
        # then we consider that the request was to delete the bookmark
        if not created:
            favourite_topic.delete()
        return redirect(request.META['HTTP_REFERER'])


class SubscribeView(View):
    """
    View to check, is user already in user subscriptions, if not - it add user to list, if yes - delete user from list
    """
    model = Subscribe

    def get(self, request, pk):
        user = auth.get_user(request)
        on_whom = User.objects.get(pk=pk)
        if user == on_whom:
            pass
        else:
            subscribe, created = self.model.objects.get_or_create(who=user, on_whom=on_whom)
            if not created:
                subscribe.delete()
        return redirect(request.META['HTTP_REFERER'])


class CategoryDetailView(MultipleObjectMixin, generic.DetailView):
    """Category detail view, include all relative topics"""
    model = Category
    template_name = 'blog/category_detail.jinja'
    paginate_by = 4

    def get_context_data(self, **kwargs):
        object_list = Topic.objects.filter(category=self.get_object())
        context = super().get_context_data(object_list=object_list)
        context['category'] = self.get_object()
        return context


class TopicSearchView(generic.ListView):
    """Search by name in all topics"""
    model = Topic
    template_name = 'blog/topic_search.jinja'
    paginate_by = 2
    # form_class = TopicSearchForm

    def get_queryset(self):
        qs = super().get_queryset()

        if self.request.GET.get('q'):
            qs = qs.filter(name__icontains=self.request.GET.get('q'))
        return qs


class ChangeLanguageView(generic.RedirectView):
    """View to change language, depending on signs in referrer url"""
    # def get(self, request, *args, **kwargs):
    #     response = super().get(request, *args, **kwargs)
    #     # if self.kwargs.get('code'):
    #     #     response.set_cookie('lang', self.kwargs.get('code'))
    #     # return response

    def get_redirect_url(self, *args, **kwargs):
        path = urlparse(self.request.META.get('HTTP_REFERER')).path
        if path[1:3] == 'uk':
            path = path[3:]
        if self.kwargs.get('code') == 'uk':
            # new_path = f"/{self.request.COOKIES.get('lang')}{p}"
            new_path = f"/{self.kwargs.get('code')}{path}"
            return new_path
        else:
            return path
