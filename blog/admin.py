from django.contrib import admin
from django import forms
from blog.models import Topic, Category, Review, FavouriteTopic, Subscribe
from treebeard.forms import movenodeform_factory
from treebeard.admin import TreeAdmin


class CategoryAdmin(TreeAdmin):
    form = movenodeform_factory(Category)
    prepopulated_fields = {'slug': ('name',)}


class TopicAdminForm(forms.ModelForm):

    class Meta:
        model = Topic
        fields = '__all__'


class TopicAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}
    readonly_fields = ('creation_date', 'last_change_date')
    form = TopicAdminForm

    def save_model(self, request, obj, form, change):
        # print(dir(form.instance))
        # number = form.instance.review_set.count()
        # print(number)
        obj.author = request.user
        super().save_model(request, obj, form, change)


class FavouriteTopicAdmin(admin.ModelAdmin):
    list_display = ('topic', 'user')


class ReviewAdmin(admin.ModelAdmin):
    readonly_fields = ('creation_date', 'last_change_date')


admin.site.register(Topic, TopicAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(FavouriteTopic, FavouriteTopicAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Subscribe)
