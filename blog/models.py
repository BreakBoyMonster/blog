from django.db import models
from django.utils.timezone import now
from profiles.models import User
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from project.utils import send_link_for_subscribers
from filer.fields.image import FilerImageField

from treebeard.mp_tree import MP_Node


class Category(MP_Node):
    """Category class, inherit from MP_Node for nesting category in category"""
    name = models.CharField(verbose_name=_('category'), max_length=150,
                            unique=True, help_text=_('category'))
    slug = models.SlugField(max_length=200, unique=True)
    # parent = models.ForeignKey('self', verbose_name=_('Подкатегория'), on_delete=models.SET_NULL,
    #                            null=True, blank=True, related_name='subcategory')

    node_order_by = ['name']

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


# def photo_upload_to(topic, photo):
#     """Метод для формирования имен картинок для статей и их сохранения в отдельной папке"""
#     return f"topics/{topic.author}_{topic.name}.{photo.split('.')[-1]})"


class Topic(models.Model):
    """Class Topic"""
    name = models.CharField(verbose_name=_('topic'), max_length=200, help_text=_('topic'))
    description = models.CharField(verbose_name=_('short description'), max_length=250,
                                   blank=True, null=True, help_text=_('short description'))
    content = RichTextUploadingField(verbose_name=_('content'), max_length=20000,
                                     blank=True, null=True, help_text=_('content'))
    recommended_topics = models.ManyToManyField('self', verbose_name=_('recommended topic'),
                                                blank=True, related_name='recommendations')
    creation_date = models.DateTimeField(verbose_name=_('creation date'), auto_now_add=True)
    last_change_date = models.DateTimeField(verbose_name=_('change date'), default=now)
    author = models.ForeignKey(User, verbose_name=_('author'), blank=True, null=True, on_delete=models.SET_NULL)
    category = models.ManyToManyField(Category, verbose_name=_('category'), related_name='categories', blank=True)
    # photo = models.ImageField(verbose_name=_('Превью'), upload_to=photo_upload_to, blank=True, null=True)
    photo = FilerImageField(verbose_name=_('photo'), null=True, blank=True,
                            on_delete=models.SET_NULL, related_name='photo')
    slug = models.SlugField(max_length=400, unique=True)
    alt = models.CharField(max_length=100, blank=True)
    rating = models.FloatField(verbose_name=_('rating'), default=0, blank=True)
    number_of_reviews = models.PositiveIntegerField(verbose_name=_('number of reviews'), default=0, blank=True)
    number_of_likes = models.PositiveIntegerField(verbose_name=_('number of likes'), default=0, blank=True)

    @staticmethod
    def get_absolute_url():
        return reverse('blog:blog')

    @staticmethod
    def get_number_of_reviews_and_count_topic_rating(sender, instance, **kwargs):
        """
        Function for recalculating the number of reviews and the average rating.
        Called on the post_save signal from Review
        """
        instance.topic.number_of_reviews = sender.objects.filter(topic=instance.topic).count()
        instance.topic.rating = Topic.objects.aggregate(rating=models.Avg('review__mark')).get('rating', 0)
        instance.topic.save()

    @staticmethod
    def get_number_of_likes(sender, instance, **kwargs):
        """
         Post_save method to count single topic likes. Calling after saving FavouriteTopic
        """
        # instance.topic.number_of_likes = sender.objects.filter(topic=instance.topic).count()
        try:
            Topic.objects.filter(id=instance.topic.id).update(
                number_of_likes=sender.objects.filter(topic=instance.topic).count())
        except Topic.DoesNotExist:
            pass

    @staticmethod
    def send_link_on_new_topic_to_subscribers(sender, instance, created, **kwargs):
        """
        Post_save method to check is last topic save the creation,
        if yes - send link for this topic to all author subscribers
        """
        if created:
            for subscription in Subscribe.objects.select_related('who').filter(on_whom=instance.author):
                message = "Hello, {}, {} created new article! Check it in our blog by following this link - \
                       http://127.0.0.1:8000/blog/topic/{}/".format(subscription.who, instance.author, instance.slug)
                send_link_for_subscribers(to_email=subscription.who, message=message)

    class Meta:
        verbose_name = _('topic')
        verbose_name_plural = _('topics')
        unique_together = ('name', 'author',)
        ordering = ('name', )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.last_change_date = now()
        super().save(*args, **kwargs)


class FavouriteTopic(models.Model):
    """Класс favourite topic (user -> topic)"""
    topic = models.ForeignKey(Topic, verbose_name=_('topic'), on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name=_('user'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('favourite topic')
        verbose_name_plural = _('favourite topics')
        # db_table = 'favourite_topic'

    def __str__(self):
        return self.topic.name

    # def save(self, *args, **kwargs):
    #     self.topic.number_of_likes = FavouriteTopic.objects.filter(topic=self.topic).count() + 1
    #     self.topic.save()
    #     super().save(*args, **kwargs)


class Review(models.Model):
    """Class review for topic (review -> topic)"""
    RATING = (
        (0, _('awful')),
        (1, _('bad')),
        (2, _('acceptable')),
        (3, _('good')),
        (4, _('very good')),
        (5, _('excellent'))
    )
    mark = models.PositiveSmallIntegerField(verbose_name=_('mark'), choices=RATING, default=3)
    comment = models.CharField(verbose_name=_('comment'), max_length=500)
    user = models.ForeignKey(User, verbose_name=_('user'), null=True, blank=True,
                             on_delete=models.SET_NULL)
    topic = models.ForeignKey(Topic, verbose_name=_('topic'), related_name='reviews',
                              null=True, on_delete=models.SET_NULL)
    creation_date = models.DateField(verbose_name=_('creation date'), auto_now_add=True)
    last_change_date = models.DateTimeField(verbose_name=_('change date'), auto_now=True)

    class Meta:
        verbose_name = _('review')
        verbose_name_plural = _('reviews')
        ordering = ('-creation_date',)

    def __str__(self):
        user = self.user if self.user else _('anonymous user')
        return f"{user} - {self.topic}"


class Subscribe(models.Model):
    """Subscription class (who(user) -> on_whom(user))"""
    who = models.ForeignKey(User, verbose_name=_('who'), related_name='who', on_delete=models.CASCADE)
    on_whom = models.ForeignKey(User, verbose_name=_('on whom'), related_name='on_whom', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('subscription')
        verbose_name_plural = _('subscriptions')

    def __str__(self):
        return f"{self.who} - {self.on_whom}"


"""Signal to recalculate the number of reviews and the average article rating after adding a review"""
models.signals.post_save.connect(Topic.get_number_of_reviews_and_count_topic_rating, sender=Review)

"""A pair of signals for recalculating the number of likes after saving and after deleting the like"""
models.signals.post_save.connect(Topic.get_number_of_likes, sender=FavouriteTopic)
models.signals.post_delete.connect(Topic.get_number_of_likes, sender=FavouriteTopic)

"""Signal to send a message to subscribers with a link to a new article from the author"""
models.signals.post_save.connect(Topic.send_link_on_new_topic_to_subscribers, sender=Topic)
