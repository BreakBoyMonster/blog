from django import template
from django_jinja import library
from blog.models import FavouriteTopic


register = template.Library()


# @register.filter
@library.filter
def is_favourite(value, request):
    """
    Функция вызывается из шаблонов, применяется как фильтр, чтобы узнать, находится ли статья в избранном

    args:
        value - принимает конкретную статью;
        request - передаем запрос из шаблона;

    vars:
        user - достаем текущего юзера из request

    return:
        Если нашли - возвращаем True, если нет - False

    Ссылка на документацию:
        https://docs.djangoproject.com/en/1.8/howto/custom-template-tags/#writing-custom-template-filters
    """

    return FavouriteTopic.objects.filter(topic=value, user=request.user).exists()
