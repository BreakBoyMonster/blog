from django.template import Context
from django.template.loader import get_template

from blog.forms import TopicSearchForm


def search_form():
    template = get_template('blog/search_form.jinja')
    return template.render(Context({'form': TopicSearchForm}))

