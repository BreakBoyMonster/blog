from django.urls import path
from blog.views import (BlogView, TopicView, FavouriteTopicView,
                        CategoryDetailView, SubscribeView,
                        TopicSearchView, ChangeLanguageView
                        )


urlpatterns = [
    path('change-language/<str:code>/', ChangeLanguageView.as_view(), name='change-language'),
    path('topic/<slug:slug>/favourite/', FavouriteTopicView.as_view(), name='topic-favourite'),
    path('topic/<slug:slug>/', TopicView.as_view(), name='topic-detail'),
    path('subscribe/<int:pk>/', SubscribeView.as_view(), name='subscribe'),
    path('category/<slug:slug>/', CategoryDetailView.as_view(), name='category'),
    path('topic_search/', TopicSearchView.as_view(), name='topic-search'),
    path('', BlogView.as_view(), name='blog'),

]
